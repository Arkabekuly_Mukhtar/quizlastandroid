package com.mukhtar.lastquiz_android2;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment implements View.OnClickListener{
    FragmentTransaction fragmentTransaction;

    int currentRow = 0; int currentCol = 0;
    public static int [][] m ;
    Bitmap manImage;
    Man man;
    DrawView drawView ;


    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_second, null);

        manImage = BitmapFactory.decodeResource(getResources(), R.drawable.sprite);
        man = new Man(manImage);
        drawView = new DrawView(getActivity(),man);
        ((FrameLayout)v.findViewById(R.id.convas_container)).addView(drawView);
        Button up = (Button)v.findViewById(R.id.up);
        Button down = (Button)v.findViewById(R.id.down);
        Button left = (Button)v.findViewById(R.id.left);
        Button right = (Button)v.findViewById(R.id.right);

        up.setOnClickListener(this);
        down.setOnClickListener(this);
        left.setOnClickListener(this);
        right.setOnClickListener(this);

        return v;
    }



    @Override
    public void onClick(View v) {
        if(!man.isMoving){
            switch (v.getId()){
                case R.id.up:
                    if((currentRow-1)>=0&&m[currentRow - 1][currentCol] == 1){
                        man.moveUp();
                        currentRow--;
                    }
                    break;
                case R.id.down:
                    if((currentRow + 1)<=m.length-1&&m[currentRow + 1][currentCol] == 1){
                        man.moveDown();
                        currentRow++;
                    }
                    break;
                case R.id.left:
                    if((currentCol - 1)>=0&&m[currentRow ][currentCol - 1] == 1){
                        man.moveLeft();
                        currentCol--;
                    }
                    break;
                case R.id.right:
                    if((currentCol + 1)<=m[0].length-1&&m[currentRow][currentCol + 1] == 1){
                        man.moveRight();
                        currentCol++;
                    }
                    break;

            }
            if(currentCol==m[0].length-1&&currentRow==m.length-1){
                drawView.isRunning = false;
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container,new FirstFragment());
                fragmentTransaction.commit();
            }
        }

    }

}
