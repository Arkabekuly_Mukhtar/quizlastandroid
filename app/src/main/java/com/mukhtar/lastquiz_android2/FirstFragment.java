package com.mukhtar.lastquiz_android2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment implements View.OnClickListener{

    FragmentTransaction fragmentTransaction;
    public static TextView highScore;
    public static int [][] map7_10 = {
            {1,1,1,1,1,1,0},
            {1,0,1,0,1,0,0},
            {1,1,1,0,1,1,0},
            {1,0,1,1,0,1,0},
            {1,1,0,1,0,1,1},
            {0,1,0,1,0,1,0},
            {0,1,0,1,0,1,0},
            {0,1,0,1,0,1,0},
            {0,1,0,1,0,1,0},
            {0,1,0,1,0,1,1},
    };
    public static int [][] map7_4 = {
            {1,1,1,1,1,1,0},
            {1,0,1,0,1,0,0},
            {1,1,1,0,1,0,0},
            {1,0,1,1,1,1,1},
    };
    public static int [][] map5_6 = {
            {1,1,1,1,1,1},
            {1,0,1,0,1,0},
            {1,1,1,0,1,1},
            {1,0,1,1,0,1},
            {1,1,0,1,0,1},
            {0,1,0,1,0,1},

    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_first, null);
        highScore = (TextView) v.findViewById(R.id.textViewHighScore);
        if(MainActivity.highScore!=Integer.MAX_VALUE){
            highScore.setText(MainActivity.highScore+"");
        }


        (v.findViewById(R.id.buttonStart)).setOnClickListener(this);
        (v.findViewById(R.id.buttonMap7_4)).setOnClickListener(this);
        (v.findViewById(R.id.buttonMap7_10)).setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonStart:
                SecondFragment.m = map5_6;
                break;
            case R.id.buttonMap7_4:
                SecondFragment.m = map7_4;
                break;
            case R.id.buttonMap7_10:
                SecondFragment.m = map7_10;
                break;
        }
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container,new SecondFragment());
        fragmentTransaction.commit();
    }
}
