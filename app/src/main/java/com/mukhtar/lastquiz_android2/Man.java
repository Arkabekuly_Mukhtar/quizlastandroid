package com.mukhtar.lastquiz_android2;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.AsyncTask;

/**
 * Created by Bimurat Mukhtar on 22.04.2017.
 */

public class Man {
    public int currentI = 0;
    public int currentJ = 0;

    final int DIRECTION_UP = 0;
    final int DIRECTION_DOWN = 1;
    final int DIRECTION_RIGHT = 2;
    final int DIRECTION_LEFT = 3;

    int ml = DrawView.rectMultiplier;

    public Bitmap image;

    private Rect source;
    private Rect dest;

    int speed = 200;

    boolean isMoving = false;

    int pngX = 278/6;
    int pngY = 148/3;


    public Man(Bitmap image){
        this.image = image;
        pngX = image.getWidth()/6;
        pngY = image.getHeight()/3;
        source = new Rect(pngX*5,pngY,pngX*6,pngY*2);
        dest = new Rect(currentJ*ml,currentI*ml,(currentJ+1)*ml,(currentI+1)*ml);
    }

    public void moveRight(){
        if(!isMoving){
            currentJ++;
            PerformMove performMove = new PerformMove(DIRECTION_RIGHT);
            performMove.execute();
            isMoving = true;
        }
    }
    public void moveLeft(){
        if(!isMoving){
            currentJ--;
            PerformMove performMove = new PerformMove(DIRECTION_LEFT);
            performMove.execute();
            isMoving = true;
        }
    }
    public void moveDown(){
        if(!isMoving){
            currentI++;
            PerformMove performMove = new PerformMove(DIRECTION_DOWN);
            performMove.execute();
            isMoving = true;
        }
    }
    public void moveUp(){
        if(!isMoving){
            currentI--;
            PerformMove performMove = new PerformMove(DIRECTION_UP);
            performMove.execute();
            isMoving = true;
        }
    }



    public Rect getSrcRect(){
        return source;
    }

    public Rect getDestinationRect(){
        return dest;
    }

    public void updateRectangles(){
//        int ml = DrawView.rectMultiplier;
//        dest = new Rect(currentJ*ml,currentI*ml,(currentJ+1)*ml,(currentI+1)*ml);
    }


    private class PerformMove extends AsyncTask<Void, Integer, Long> {
        int direction;
        Rect [] rects;
        States states;

        public PerformMove(int dir){
            this.direction = dir;
            states = new States();
        }

        @Override
        protected void onPreExecute() {
            dest = new Rect(currentJ*ml,currentI*ml,(currentJ+1)*ml,(currentI+1)*ml);
            switch (direction){
                case DIRECTION_DOWN:
                    rects = states.getDownRects();
                    break;
                case DIRECTION_UP:
                    rects = states.getUpRects();
                    break;
                case DIRECTION_RIGHT:
                    rects = states.getRightRects();
                    break;
                case DIRECTION_LEFT:
                    rects = states.getLeftRects();
                    break;
            }
            super.onPreExecute();
        }

        protected Long doInBackground(Void... voids) {

            for(int i = 0; i<rects.length;i++){
                publishProgress(i);
                try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return 0L;
        }

        protected void onProgressUpdate(Integer... integers) {
            int currentNumber = integers[0];
            int len = rects.length;
            source = rects[currentNumber];

            switch (direction){
                case DIRECTION_DOWN:
                    dest = new Rect(currentJ*ml,(currentI-1)*ml+(ml/len*currentNumber),
                            (currentJ+1)*ml,currentI*ml+(ml/len*currentNumber));
                    break;
                case DIRECTION_UP:
                    dest = new Rect(currentJ*ml,(currentI+1)*ml-(ml/len*currentNumber),
                            (currentJ+1)*ml,(currentI+2)*ml-(ml/len*currentNumber));
                    break;
                case DIRECTION_RIGHT:
                    dest = new Rect((currentJ-1)*ml+(ml/len*currentNumber),currentI*ml,
                            currentJ*ml+(ml/len*currentNumber),(currentI+1)*ml);
                    break;
                case DIRECTION_LEFT:
                    dest = new Rect((currentJ+1)*ml-(ml/len*currentNumber),currentI*ml,
                            (currentJ+2)*ml-(ml/len*currentNumber),(currentI+1)*ml);
                    break;
            }

        }

        protected void onPostExecute(Long result) {
            isMoving = false;
        }
    }





    public class States {
        public Rect[] getRightRects() {
            Rect[] res = {
                    new Rect(pngX * 5, 0, pngX * 6, pngY),
                    new Rect(pngX * 5, pngY, pngX * 6, pngY * 2),
                    new Rect(pngX * 5, pngY * 2, pngX * 6, pngY * 3)
            };
            return res;
        }

        public Rect[] getLeftRects() {
            Rect[] res = {
                    new Rect(pngX * 4, 0, pngX * 5, pngY),
                    new Rect(pngX * 4, pngY, pngX * 5, pngY * 2),
                    new Rect(pngX * 4, pngY * 2, pngX * 5, pngY * 3)
            };
            return res;
        }


        public Rect[] getUpRects() {
            Rect[] res = {
                    new Rect(pngX * 2, 0, pngX * 3, pngY),
                    new Rect(pngX * 3, 0, pngX * 4, pngY),
                    new Rect(pngX * 2, pngY, pngX * 3, pngY * 2),
                    new Rect(pngX * 3, pngY, pngX * 4, pngY * 2),
            };
            return res;
        }
        public Rect[] getDownRects(){
            Rect [] res = {
                    new Rect(pngX,pngY,pngX*2,pngY*2),
                    new Rect(0,0,pngX,pngY),
                    new Rect(0,pngY,pngX,pngY*2),
                    new Rect(pngX,0,pngX*2,pngY),
            };
            return res;
        }
    }


}
