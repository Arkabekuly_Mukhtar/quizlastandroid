package com.mukhtar.lastquiz_android2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * Created by Bimurat Mukhtar on 22.04.rectMultiplier17.
 */

public class DrawView extends android.view.SurfaceView
        implements SurfaceHolder.Callback{

    public static int rectMultiplier = 60;
    boolean isRunning = false;
    SurfaceHolder surfaceHolder;

    Man man;

    public DrawView(Context context,Man man) {
        super(context);
        this.man = man;
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        isRunning = true;

        new Thread(new Runnable() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                while(isRunning){
                    Canvas canvas = surfaceHolder.lockCanvas();
                    if(canvas == null) continue;
                    drawRect(canvas);
                    drawMan(canvas);
                    surfaceHolder.unlockCanvasAndPost(canvas);
                    long time = System.currentTimeMillis() - startTime;
                    if(time<MainActivity.highScore){
                        MainActivity.highScore = time;
                    }
                    try {
                        Thread.sleep(50);
                        update();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isRunning = false;
    }

    public void update(){
        if(man!=null){
            man.updateRectangles();
        }
    }

    public void drawRect(Canvas canvas){
        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setStrokeWidth(1);
        canvas.drawColor(Color.WHITE);

        for(int i = 0; i<SecondFragment.m.length; i++){
            for(int j = 0; j<SecondFragment.m[0].length;j++){
                if(SecondFragment.m[i][j]==1){
                    p.setColor(Color.GREEN);
                    canvas.drawRect(j*rectMultiplier,i*rectMultiplier,(j+1)*rectMultiplier,(i+1)*rectMultiplier,p);
                }else{
                    p.setColor(Color.RED);
                    canvas.drawRect(j*rectMultiplier,i*rectMultiplier,(j+1)*rectMultiplier,(i+1)*rectMultiplier,p);
                }
            }
        }
    }

    public void drawMan(Canvas canvas){
        man.updateRectangles();
        canvas.drawBitmap(man.image, man.getSrcRect(),
                man.getDestinationRect(), null);

    }
}